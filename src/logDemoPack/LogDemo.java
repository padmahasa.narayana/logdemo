/**
 * 
 */
package logDemoPack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author padmahasa.narayana
 *
 */
public class LogDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		int iter = 0;
		while (iter <= 20) {
			logger.info("Using XML configuration is the easiest way of logging");
			logger.info("This logger is to test the functionality of logger.");
			logger.info("The logger is working perfectly");
			logger.error("Only error logs should be sent to FileErrorLogs file");

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			iter++;
		}
	}

}
